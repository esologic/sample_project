from distutils.core import setup

setup(
    name="sample_project",
    version="1.0",
    description="This is a sample project!",
    author="Devon Bray",
    author_email="dev@esologic.com",
    url="www.esologic.com",
    package_dir={"sample_project": "./src"},
    packages=["sample_project"],
)
