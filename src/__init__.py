# -*- coding: utf-8 -*-

"""Top-level package for Sample Project."""

__author__ = """Devon Bray"""
__email__ = "dev@esologic.com"
__version__ = "1.0.0"

from . sample_project import print_quote
