"""
Functions for this sample module
"""


def print_quote() -> None:
    """
    Print a good quote with the `print` function.
    :return: None
    """
    print("If they can get you asking the wrong questions, they don't have to worry about answers.")
